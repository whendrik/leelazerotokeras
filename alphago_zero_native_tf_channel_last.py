from tensorflow.keras.layers import *
from tensorflow.keras.models import Model



'''
Based on AlphaGo Zero implementation of Max Pumperla
from
https://github.com/maxpumperla/deep_learning_and_the_game_of_go/tree/master/code/dlgo/networks

with slight modifications to make consistent with Leela-Zero Weights & 
exportable with tensorflow.js.

Thx Max!

'''



'''The dual residual architecture is the strongest
of the architectures tested by DeepMind for AlphaGo
Zero. It consists of an initial convolutional block,
followed by a number (39 for the strongest, 19 as 
baseline) of residual blocks. The network is topped
off by two "heads", one to predict policies and one
for value functions.
'''

def dual_residual_network(input_shape, blocks=20, filters=256):
	inputs = Input(shape=input_shape)
	first_conv = conv_bn_relu_block(name="init", filters=filters)(inputs)
	res_tower = residual_tower(blocks=blocks, filters=filters)(first_conv)
	policy = policy_head()(res_tower)
	value = value_head()(res_tower)
	return Model(inputs=inputs, outputs=[policy, value])

def conv_bn_relu_block(name, activation=True, filters=256, kernel_size=(3,3), 
					   strides=(1,1), padding="same", init="zeros"):
	def f(inputs):
		conv = Conv2D(filters=filters, 
					  kernel_size=kernel_size,
					  strides=strides,
					  padding=padding,
					  kernel_initializer=init,
					  data_format='channels_last',
					  name="{}_conv_block".format(name))(inputs)
		batch_norm = BatchNormalization(axis=-1, center=False, scale=False, epsilon=1e-5, name="{}_batch_norm".format(name))(conv)
		return Activation("relu", name="b_{}_relu".format(name))(batch_norm) if activation else batch_norm
	return f    


def residual_block(block_num, filters, **args):
	def f(inputs):
		res = conv_bn_relu_block(name="residual_1_{}".format(block_num), activation=True, filters=filters, **args)(inputs)
		res = conv_bn_relu_block(name="residual_2_{}".format(block_num) , activation=False, filters=filters, **args)(res)
		res = add([inputs, res], name="add_{}".format(block_num))
		return Activation("relu", name="b_{}_relu".format(block_num))(res) 
	return f


def residual_tower(blocks, filters, **args):
	def f(inputs):
		x = inputs
		for i in range(blocks):
			x = residual_block(block_num=i, filters=filters, **args)(x)
		return x
	return f

def policy_head():
	def f(inputs):
		conv = Conv2D(filters=2, 
					  kernel_size=(1, 1),
					  strides=(1, 1),
					  data_format="channels_last",
					  padding="same",
					  name="policy_head_conv_block")(inputs)
		batch_norm = BatchNormalization(axis=-1, center=False, scale=False, epsilon=1e-5, name="policy_head_batch_norm")(conv)
		activation = Activation("relu", name="policy_head_relu")(batch_norm)
		permute = Permute(dims=(3,1,2))(activation)
		flatten = Flatten(data_format='channels_last', name="policy_flatten")(permute)
		return Dense(units=19*19 +1, name="policy_head_dense")(flatten)
	return f    

def value_head():
	def f(inputs):
		conv = Conv2D(filters=1, 
					  kernel_size=(1, 1),
					  strides=(1, 1),
					  data_format="channels_last",
					  padding="same",
					  name="value_head_conv_block")(inputs)
		batch_norm = BatchNormalization(axis=-1, center=False, scale=False, epsilon=1e-5, name="value_head_batch_norm")(conv)
		activation = Activation("relu", name="value_head_relu")(batch_norm)
		permute = Permute(dims=(3,1,2))(activation)
		flatten = Flatten(data_format='channels_last', name="value_head_flatten")(permute)
		dense =  Dense(units=256, name="value_head_dense", activation="relu")(flatten)
		return Dense(units= 1, name="value_head_output", activation="tanh")(dense)
	return f   

#model = dual_residual_network( (19,19,18) , blocks=1, filters=8)
#model.predict( np.zeros([5,19,19,18])) 

#from tensorflow.keras.utils import plot_model
#plot_model(model, to_file='keras_model_4x32.png')