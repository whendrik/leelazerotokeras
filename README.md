# Leelazero to Keras

Created by Willem Hendriks, whendrik@gmail.com.
Thx to Max Pumperla for code & coaching.

## Usage

```
usage: export_to_keras.py weights_file
```

## Example Code

`Keras Model Inspection.ipynb` within the [notebooks](notebooks) directory contains examples how to load and use the network. 

Model can be loaded in Keras with
```
from tensorflow.keras.models import load_model

model = load_model("leelazero_10x128.h5")
```

Say `N` board positions, `board_positions`, have been encoded in [right format](https://github.com/leela-zero/leela-zero#weights-format);

```
board_positions.shape
(N, 19, 19, 18)
```
and

```
network_output = model.predict(board_positions)
```

`network_output` contains a list of length 2, where

1. First element, a `N x 362` vector. For each board posisiont `N`, it contains 362 logits probabilities - for all 361 intersection and the pass move.
2. Second element, a `N x 1`. For each board posisiont `N`, a tanh nonlinearity scalar in range [-1,1], which can be transformed to the probability to win for player to move.

The 2nd output, `tanh_output`, can be converted to a probabilitie with `(1.0 + np.tanh(tanh_output)) / 2.0`.

## Details About Implemented Keras Architecture & Leela Zero Weight Format

The Leela-Zero Keras Implementation uses `data_format='channels_last'`. Weights cannot be directly shoved into the Keras architecture, and need `np.reshape()` & `np.transpose()` operations to make them compatible. This is done by the script.

### Detailed Description for Leela Zero Weight file

Explained-by-Example, for a `1x8` network (1 Block, 8 Filters in Conv2D Blocks) the file contains the following lines:

1. Version number
2. Initial Block - Conv2D Layer, Weights - 18 * 3*3 * (8=filters)
3. Initial Block - Conv2D Layer, Biases - for 8 filters
4. Initial Block - Batch Normalisation, Means
5. Initial Block - Batch Normalisation, Variances
---
6. 1st Block - Conv2D Layer, Weights - (8=filters) * 3*3 * (8=filters)
7. 1st Block - Conv2D Layer, Biases - for 8 filters
8. 1st Block - Batch Normalisation, Means
9. 1st Block - Batch Normalisation, Variances
10. 1st Block - Conv2D Layer, Weights - (8=filters) * 3*3 * (8=filters)
11. 1st Block - Conv2D Layer, Biases - for 8 filters
12. 1st Block - Batch Normalisation, Means
13. 1st Block - Batch Normalisation, Variances
---
14. Policy Head - Conv2D Layer, Weights - 2 * 1*1 * (8=filters)
15. Policy Head - Conv2D Layer, Biases - for 2 filters
16. Policy Head - Batch Normalisation, Means
17. Policy Head - Batch Normalisation, Variances
18. Policy Head - Dense Layer, Weights - 361+(1=pass)=362 outputs, 361 * 2 * 362
19. Policy Head - Dense Layer, Biases - 362
---
20. Value Head - Conv2D Layer, Weights - 1 * 1*1 * (8=filters)
21. Value Head - Conv2D Layer, Bias - for 1 filters
22. Value Head - Batch Normalisation, Means
23. Value Head - Batch Normalisation, Variances
24. Value Head - Dense Layer, Weights - 361*256
25. Value Head - Dense Layer, Biases - 256
26. Value Head - Dense Layer, Weights - 256
27. Value Head - Dense Layer, Bias - 1

For larger archtitectures, the 1st Block will be repeated and the filters increased.

### Weights Order

From [Leela Zero](https://github.com/leela-zero/leela-zero#weights-format) Weights Format Description we see

- The Convolution Weights are in `[output, input, filter_size, filter_size]` order.
- The Dense Layer Weights are in `[output, input]` order. 

To make the Convolution Weights compatible with Keras, we make use of 
```
X == np.transpose(np.transpose(X,[3,2,0,1]) ,[2, 3, 1, 0])
```

Say `weights` is a `numpy` array with the Convolution weights loaded, we make them compatible with
```
weights.shape = (filters,filters,3,3)
keras_weights = np.transpose(weights, [2,3,1,0])
```

Say `weights` is a `numpy` array with the Dense weights loaded, we make them compatible with
```
weights.shape = (256,361)
keras_weights = np.transpose(weights)
```

### Keras Conv2D layers

```
strides=(1,1), padding="same", data_format='channels_last',
```

### Keras BatchNormalisation layers

The Batch Normalisation don't have a center(=beta) and scale(=gamma). It is possible to encode the biases of previous layers, into the beta of the Batch Normalisation layers, as done with [Leela Zero](https://github.com/leela-zero/leela-zero#weights-format). This is not done in the Keras implementation. The Epsilon used is `epsilon=1e-5`.

```
axis=-1, center=False, scale=False, epsilon=1e-5,
```

### Keras Dense layers

Before Flatten the layers, [Permute()](https://keras.io/layers/core/#permute) is used to make the weights consistent with Leela-Zero format.

```
Permute(dims=(3,1,2))
```

### Keras Model Summary

For a `leelazero_1x8.h5` Keras model, a `model.summary()` will output

```
Layer (type)                    Output Shape         Param #     Connected to                     
==================================================================================================
input_1 (InputLayer)            (None, 19, 19, 18)   0                                            
__________________________________________________________________________________________________
init_conv_block (Conv2D)        (None, 19, 19, 8)    1304        input_1[0][0]                    
__________________________________________________________________________________________________
init_batch_norm (BatchNormaliza (None, 19, 19, 8)    16          init_conv_block[0][0]            
__________________________________________________________________________________________________
b_init_relu (Activation)        (None, 19, 19, 8)    0           init_batch_norm[0][0]            
__________________________________________________________________________________________________
residual_1_0_conv_block (Conv2D (None, 19, 19, 8)    584         b_init_relu[0][0]                
__________________________________________________________________________________________________
residual_1_0_batch_norm (BatchN (None, 19, 19, 8)    16          residual_1_0_conv_block[0][0]    
__________________________________________________________________________________________________
b_residual_1_0_relu (Activation (None, 19, 19, 8)    0           residual_1_0_batch_norm[0][0]    
__________________________________________________________________________________________________
residual_2_0_conv_block (Conv2D (None, 19, 19, 8)    584         b_residual_1_0_relu[0][0]        
__________________________________________________________________________________________________
residual_2_0_batch_norm (BatchN (None, 19, 19, 8)    16          residual_2_0_conv_block[0][0]    
__________________________________________________________________________________________________
add_0 (Add)                     (None, 19, 19, 8)    0           b_init_relu[0][0]                
                                                                 residual_2_0_batch_norm[0][0]    
__________________________________________________________________________________________________
b_0_relu (Activation)           (None, 19, 19, 8)    0           add_0[0][0]                      
__________________________________________________________________________________________________
value_head_conv_block (Conv2D)  (None, 19, 19, 1)    9           b_0_relu[0][0]                   
__________________________________________________________________________________________________
policy_head_conv_block (Conv2D) (None, 19, 19, 2)    18          b_0_relu[0][0]                   
__________________________________________________________________________________________________
value_head_batch_norm (BatchNor (None, 19, 19, 1)    2           value_head_conv_block[0][0]      
__________________________________________________________________________________________________
policy_head_batch_norm (BatchNo (None, 19, 19, 2)    4           policy_head_conv_block[0][0]     
__________________________________________________________________________________________________
value_head_relu (Activation)    (None, 19, 19, 1)    0           value_head_batch_norm[0][0]      
__________________________________________________________________________________________________
policy_head_relu (Activation)   (None, 19, 19, 2)    0           policy_head_batch_norm[0][0]     
__________________________________________________________________________________________________
permute_1 (Permute)             (None, 1, 19, 19)    0           value_head_relu[0][0]            
__________________________________________________________________________________________________
permute (Permute)               (None, 2, 19, 19)    0           policy_head_relu[0][0]           
__________________________________________________________________________________________________
value_head_flatten (Flatten)    (None, 361)          0           permute_1[0][0]                  
__________________________________________________________________________________________________
policy_flatten (Flatten)        (None, 722)          0           permute[0][0]                    
__________________________________________________________________________________________________
value_head_dense (Dense)        (None, 256)          92672       value_head_flatten[0][0]         
__________________________________________________________________________________________________
policy_head_dense (Dense)       (None, 362)          261726      policy_flatten[0][0]             
__________________________________________________________________________________________________
value_head_output (Dense)       (None, 1)            257         value_head_dense[0][0]           
==================================================================================================
Total params: 357,208
Trainable params: 357,154
Non-trainable params: 54
__________________________________________________________________________________________________
```

### Initalization of Weights

Currently, initialization is done with zeros, to be comparible with `tensorflow.js`, and not suitable for training.

### Current Visualisation Keras Model

#### 1x8
![1x8](images/keras_model_1x8.png)

#### 4x32
![4x32](images/keras_model_4x32.png)