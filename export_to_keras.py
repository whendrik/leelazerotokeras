#!/usr/bin/env python3
import os
import argparse
import numpy as np
import re

from alphago_zero_native_tf_channel_last import *

DEBUG = False

"""
Parsing Leela-Zero weights file

"""

parser = argparse.ArgumentParser(description='Convert LeelaZero weights to Keras')
parser.add_argument('weights_file', help='LeelaZero weights')
args = parser.parse_args()

filters = 0
blocks = 0
with open(args.weights_file, 'r') as f:
	weights = []
	for e, line in enumerate(f):
		if e == 0:
			#Version
			print("Version", line.strip())
			if line != '1\n':
				raise ValueError("Unknown version {}".format(line.strip()))
		else:
			weights.append(list(map(float, line.split(' '))))
		if e == 2:
			channels = len(line.split(' '))
			print("Channels", channels)
			filters = channels

	blocks = e - (4 + 14)
	if blocks % 8 != 0:
		raise ValueError("Inconsistent number of weights in the file")
	blocks //= 8
	print("Blocks", blocks)

def layer_type(i,blocks=blocks, filters=filters):
	if i == 0:
		return "========= {} =========\n  Initial Block - Conv2D Layer, Weights - 18 * 3*3 * ({}=filters)".format("Initial Conv-Block",filters)
	if i == 1:
		return "  Initial Block - Conv2D Layer, Biases - {}".format(filters)
	if i == 2:
		return "  Initial Block - Batch Normalisation, Means"
	if i == 3:
		return "  Initial Block - Batch Normalisation, Variances"
	if i > 3 and i < 4 + blocks * 8 and (i+4) % 4 == 0:
		header = '-----------------------\n'
		if (i+4)% 8 == 0:
			header = "========= {}th BLOCK =========\n".format(int((i+4)/ 8))
		return "  {}  Block - Conv2D Layer, Weights - ({}=filters) * 3*3 * ({}=filters)".format(header,filters, filters)
	if i > 3 and i < 4 + blocks * 8 and (i+4) % 4 == 1:
		return "  Block - Conv2D Layer, Biases - {}".format(filters)
	if i > 3 and i < 4 + blocks * 8 and (i+4) % 4 == 2:
		return "  Block - Batch Normalisation, Means"
	if i > 3 and i < 4 + blocks * 8 and (i+4) % 4 == 3:
		return "  Block - Batch Normalisation, Variances"
	if i == 4 + blocks * 8:
		return "========= POLICY HEAD =========\n Policy Head - Conv2D Layer, Weights - 2 Filters of 1*1 - 2 * 1*1 * ({}=filters)".format(filters)
	if i == 4 + blocks * 8 + 1:
		return "  Policy Head - Conv2D Layer, Biases - for 2 filters"
	if i == 4 + blocks * 8 + 2:
		return "  Policy Head - Batch Normalisation, Means"
	if i == 4 + blocks * 8 + 3:
		return "  Policy Head - Batch Normalisation, Variances"
	if i == 4 + blocks * 8 + 4:
		return "  Policy Head - Dense Layer - Fully Connected to 361+(1=pass)=362 outputs, 361*2*362"
	if i == 4 + blocks * 8 + 5:
		return "  Policy Head - biases for 361+1=362 outputs"
	if i == 4 + blocks * 8 + 6:
		return "========= VALUE HEAD =========\n  Value Head - Conv2D Layer, Weights -  of 1*1 - 1 * 1*1 * ({}=filters)".format(filters)
	if i == 4 + blocks * 8 + 7:
		return "  Value Head - Conv2D Layer, Biases - for 1 filters"
	if i == 4 + blocks * 8 + 8:
		return "  Value Head - Batch Normalisation, Means"
	if i == 4 + blocks * 8 + 9:
		return "  Value Head - Batch Normalisation, Variances"	
	if i == 4 + blocks * 8 + 10:
		return "  Value Head - Dense Layer, Weights - 361*256"	
	if i == 4 + blocks * 8 + 11:
		return "  Value Head - Dense Layer, Biases - 256"
	if i == 4 + blocks * 8 + 12:
		return "  Value Head - Dense Layer, Weights"
	if i == 4 + blocks * 8 + 13:
		return "  Value Head - Dense Layer, Bias"	
	return "  "


if DEBUG:
	for i,weight_row in enumerate(weights):
		print(layer_type(i,blocks))
		print(len(weight_row))

"""
Create Keras Model
"""

model = dual_residual_network( (19,19,18) , blocks=blocks, filters=filters)
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
print("=== Keras {}x{} model created ===".format(blocks,filters))


def reshape_conv_weights(weights, filters=filters, initial_block=False):
	"""
	The convolution weights are in [output, input, filter_size, filter_size] order, 
	the fully connected layer weights are in [output, input] order. 
	The residual tower is first, followed by the policy head, 
	and then the value head. All convolution filters are 3x3 except for 
	the ones at the start of the policy and value head, 
	which are 1x1 (as in the paper).

	from tfprocess.py:
	
    # TF (kYXInputOutput)
    # [filter_height, filter_width, in_channels, out_channels]
    #
    # Leela/cuDNN/Caffe (kOutputInputYX)
    # [output, input, filter_size, filter_size]

    Note that:

    X == np.transpose(np.transpose(X,[3,2,0,1]) ,[2, 3, 1, 0])
	"""
	if initial_block:
		weights.shape = (filters,18,3,3)
	else:
		weights.shape = (filters,filters,3,3)
	return np.transpose(weights, [2,3,1,0])

def reshape_head_conv_weights(weights, filters=filters, policy=True):
	"""
	The convolution weights are in [output, input, filter_size, filter_size] order, 
	the fully connected layer weights are in [output, input] order. 
	The residual tower is first, followed by the policy head, 
	and then the value head. All convolution filters are 3x3 except for 
	the ones at the start of the policy and value head, 
	which are 1x1 (as in the paper).
	"""
	if policy:
		weights.shape = (2,filters,1,1)
	else:
		weights.shape = (1,filters,1,1)
	return np.transpose(weights, [2,3,1,0])

def reshape_head_dense_weights(weights, filters=filters, policy=True):
	"""
	The convolution weights are in [output, input, filter_size, filter_size] order, 
	the fully connected layer weights are in [output, input] order. 
	The residual tower is first, followed by the policy head, 
	and then the value head. All convolution filters are 3x3 except for 
	the ones at the start of the policy and value head, 
	which are 1x1 (as in the paper).
	"""
	if policy:
		weights.shape = (362,722)
	else:
		weights.shape = (256,361)
	return np.transpose(weights)

####

def get_layer_by_regex(j, regexpr):
	count = j
	for i,layer in enumerate(model.layers):
		regex_compiled = re.compile(regexpr)
		if regex_compiled.match(layer.name):
			count -= 1
			if count == 0:
				return i

def get_conv_block_layer(j):
	return get_layer_by_regex(j, "residual_\d+_\d+_conv_block")

def get_batch_norm_layer(j):
	return get_layer_by_regex(j, "residual_\d+_\d+_batch_norm")

def get_value_head_conv_block():
	return get_layer_by_regex(1, "value_head_conv_block")

def get_policy_head_conv_block():
	return get_layer_by_regex(1, "policy_head_conv_block")

def get_value_head_batchnorm():
	return get_layer_by_regex(1, "value_head_batch_norm")

def get_policy_head_batchnorm():
	return get_layer_by_regex(1, "policy_head_batch_norm")

def get_policy_head_dense():
	return get_layer_by_regex(1, "policy_head_dense")

def get_value_head_dense():
	return get_layer_by_regex(1, "value_head_dense")

def get_value_head_output():
	return get_layer_by_regex(1, "value_head_output")

####

def shove_conv_weights(model, i ,weights, biases):
	if i == 0:
		model.layers[1].set_weights([weights, biases])
		print("[*] Shoved! - ConvBlock Layer 1 - Initial - {}".format(model.layers[1].name))
	else:
		conv_block = int(i/4)
		layer_i = get_conv_block_layer(conv_block)
		model.layers[layer_i].set_weights([weights, biases])	
		print("[*] Shoved! - ConvBlock Layer {} - {}".format(layer_i, model.layers[layer_i].name))

def shove_head_conv_weights(model, i, weights, biases, policy=True):
	headname = "Value"
	layer_i = get_value_head_conv_block()
	if policy:
		layer_i = get_policy_head_conv_block()
		headname = "Policy"

	model.layers[layer_i].set_weights([weights, biases])	
	print("[*] Shoved! - ConvBlock {} Layer - {}".format(headname, model.layers[layer_i].name))

def shove_head_batchnorm_weights(model, i, means, variances, policy=True):
	layer_i = get_value_head_batchnorm()
	filters = 1
	if policy:
		layer_i = get_policy_head_batchnorm()
		filters = 2

	model.layers[layer_i].set_weights([means, variances ])
	print("[*] Shoved! - Head BatchNorm Layer {} - {}".format(layer_i, model.layers[layer_i].name))

def shove_batchnorm_weights(model, i, means, variances, filters=filters):
	if i == 2:
		model.layers[2].set_weights([means, variances ])
		print("[*] Shoved! - BatchNorm Layer 2 - Initial - {}".format(model.layers[2].name))
	else:
		conv_block = int(i/4)
		layer_i = get_batch_norm_layer(conv_block)
		model.layers[layer_i].set_weights([means, variances ])
		print("[*] Shoved! - BatchNorm Layer {} - {}".format(layer_i, model.layers[layer_i].name))

def shove_head_dense_weights(model, i, weights, biases, policy=True):
	headname = "Value"
	layer_i = get_value_head_dense()
	if policy:
		layer_i = get_policy_head_dense()
		headname = "Policy"

	model.layers[layer_i].set_weights([weights, biases])	
	print("[*] Shoved! - {} Head Dense Layer - {}".format(headname, model.layers[layer_i].name))

def shove_value_head_output(model, i, weights, biases):
	layer_i = get_value_head_output()
	model.layers[layer_i].set_weights([weights, biases])	
	print("[*] Shoved! - Value Output - {}".format(model.layers[layer_i].name))


for i,_ in enumerate(weights):
	if i % 2 == 0:
		np_weights = np.array(weights[i])
		np_weights_bias = np.array(weights[i+1])

		np_weights_reshape = np_weights
		np_weights_bias_reshape = np_weights_bias

		if i == 0: # INITIAL CONV BLOCK
			np_weights_reshape = reshape_conv_weights(np_weights, initial_block=True)
			np_weights_bias_reshape = np_weights_bias

			shove_conv_weights(model, i, np_weights_reshape, np_weights_bias_reshape)

		if i == 2: # BATCH NORMALIZATION MEAN/VAR
 			shove_batchnorm_weights(model, i, np_weights_reshape, np_weights_bias_reshape)

		if i > 3 and i < 4 + blocks * 8 and (i+4) % 4 == 0: # CONV IN BLOCKS
			np_weights_reshape = reshape_conv_weights(np_weights)
			np_weights_bias_reshape = np_weights_bias
			shove_conv_weights(model, i, np_weights_reshape, np_weights_bias_reshape)

		if i > 3 and i < 4 + blocks * 8 and (i+4) % 4 == 2: # BATCHNORM IN BLOCKS
			shove_batchnorm_weights(model, i, np_weights_reshape, np_weights_bias_reshape)

		if i == 4 + blocks * 8: # POLICY CONV HEAD
			np_weights_reshape = reshape_head_conv_weights(np_weights, policy=True)
			np_weights_bias_reshape = np_weights_bias
			shove_head_conv_weights(model, i, np_weights_reshape, np_weights_bias_reshape)
		
		if i == 4 + blocks * 8 + 2: # POLICY BATCH NORM
			shove_head_batchnorm_weights(model, i, np_weights_reshape, np_weights_bias_reshape, policy=True)

		if i == 4 + blocks * 8 + 4: # POLICY DENSE
			np_weights_reshape = reshape_head_dense_weights(np_weights, policy=True)
			np_weights_bias_reshape = np_weights_bias
			shove_head_dense_weights(model, i, np_weights_reshape, np_weights_bias_reshape, policy=True)

		if i == 4 + blocks * 8 + 6: # VALUE CONV HEAD
			np_weights_reshape = reshape_head_conv_weights(np_weights, policy=False)
			np_weights_bias_reshape = np_weights_bias
			shove_head_conv_weights(model, i, np_weights_reshape, np_weights_bias_reshape, policy=False)

		if i == 4 + blocks * 8 + 8: # VALUE BATCH NORM
			shove_head_batchnorm_weights(model, i, np_weights_reshape, np_weights_bias_reshape, policy=False)

		if i == 4 + blocks * 8 + 10: # VALUE DENSE 
			np_weights_reshape = reshape_head_dense_weights(np_weights, policy=False)
			np_weights_bias_reshape = np_weights_bias
			shove_head_dense_weights(model, i, np_weights_reshape, np_weights_bias_reshape, policy=False)

		if i == 4 + blocks * 8 + 12: # FULLY CONNECT SINGLE SCALAR
			np_weights_reshape = np_weights
			np_weights.shape = (256,1)
			np_weights_bias_reshape = np_weights_bias
			shove_value_head_output(model, i, np_weights_reshape, np_weights_bias_reshape)


model.save('{}.h5'.format(args.weights_file))
print( "Keras Model Exported to: {}.h5".format(args.weights_file))